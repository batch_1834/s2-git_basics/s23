db.singleRoom.insertOne({
	"name": "single",
	"accommodates": "2",
	"price": "1000",
	"description": "A simple room with all the basic necessities",
	"rooms_available": 10,
	"isAvalable": false

})

db.multipleRooms.insertMany([
	{
		"name": "double",
		"accommodates": "3",
		"price": 2000,
		"description": "A room with a queen sized bed perfect for a simple getaway.",
		"rooms_available": "5",
		"isAvalable": false
	},

	{
		"name": "queen",
		"accommodates": "4",
		"price": 4000,
		"description": "A room with a queen sized bed perfect for a simple getaway.",
		"rooms_available": "15",
		"isAvalable": false
	}
])


db.multipleRooms.find({"name": "double"})

db.multipleRooms.updateOne(
	{"name": "queen"},
		{
			$set:{
				"name": "queen",
				"accommodates": "4",
				"price": 4000,
				"description": "A room with a queen sized bed perfect for a simple getaway.",
				"rooms_available": "0",
				"isAvalable": false
			}

		}
)


db.multipleRooms.deleteMany({
	"name": "queen"
})


